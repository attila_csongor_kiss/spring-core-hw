package com.epam.model;

public class Mission {
	private String name;
	private int strengthNeeded;
	
	public Mission(String name, int strengthNeeded) {
		super();
		this.name = name;
		this.strengthNeeded = strengthNeeded;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getStrengthNeeded() {
		return strengthNeeded;
	}

	public void setStrengthNeeded(int strengthNeeded) {
		this.strengthNeeded = strengthNeeded;
	}

	@Override
	public String toString() {
		return "Mission [name=" + name + ", strengthNeeded=" + strengthNeeded + "]";
	}
}
