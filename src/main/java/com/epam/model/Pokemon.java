package com.epam.model;

import java.util.List;

public class Pokemon {
	public enum Region {
		KANTO, JOHTO, HOENN, SINNOH, UNOVA, KALOS
	};
	
	public enum PokeType{
		NATURE, FIRE, WATER
	}
	
	private String name;
	private int strength;
	private Person owner;
	private int generation;
	private int level;
	private List<Person> previousOwners;
	private List<Pokemon> mainEnemies;
	private PokeType pokeType;
	private Region region;
	private Mission actualMission;
	
	public Pokemon(String name, int strength, int generation,
			PokeType pokeType, Region region) {
		super();
		this.name = name;
		this.strength = strength;
		this.generation = generation;
		this.pokeType = pokeType;
		this.region = region;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getStrength() {
		return this.strength;
	}

	public void setStrength(int strength) {
		this.strength = strength;
	}

	public Person getOwner() {
		return owner;
	}

	public void setOwner(Person owner) {
		this.owner = owner;
	}

	public int getGeneration() {
		return generation;
	}

	public void setGeneration(int generation) {
		this.generation = generation;
	}

	public int getLevel() {
		return level;
	}

	public void setLevel(int level) {
		this.level = level;
	}

	public List<Person> getPreviousOwners() {
		return previousOwners;
	}

	public void setPreviousOwners(List<Person> previousOwners) {
		this.previousOwners = previousOwners;
	}

	public List<Pokemon> getMainEnemies() {
		return mainEnemies;
	}

	public void setMainEnemies(List<Pokemon> mainEnemies) {
		this.mainEnemies = mainEnemies;
	}

	public PokeType getPokeType() {
		return pokeType;
	}

	public void setPokeType(PokeType pokeType) {
		this.pokeType = pokeType;
	}

	public Region getRegion() {
		return region;
	}

	public void setRegion(Region region) {
		this.region = region;
	}

	public Mission getActualMission() {
		return actualMission;
	}

	public void setActualMission(Mission actualMission) {
		this.actualMission = actualMission;
	}

	@Override
	public String toString() {
		if(this.previousOwners != null){
		return "Pokemon [name=" + name + ", Strength=" + strength + ", owner="
				+ owner + ", generation=" + generation + ", level=" + level
				+ ", previousOwners=" + previousOwners + ", mainEnemies="
				+ mainEnemies + ", pokeType=" + pokeType + ", region=" + region
				+ ", actualMission=" + actualMission + "]";
		}else{
			return "Pokemon [name=" + name + ", Strength=" + strength + ", owner="
					+ owner + ", generation=" + generation + ", level=" + level
					+ ", previousOwners=" + "no previous owners" + ", mainEnemies="
					+ mainEnemies + ", pokeType=" + pokeType + ", region=" + region
					+ ", actualMission=" + actualMission + "]";
		}
		
	}

	
}
