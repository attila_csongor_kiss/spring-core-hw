package com.epam.model;

public class Person {
	private String name;
	private int skill;
	
	public Person(String name, int skill) {
		super();
		this.name = name;
		this.skill = skill;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public int getSkill() {
		return skill;
	}
	public void setSkill(int skill) {
		this.skill = skill;
	}
	
	@Override
	public String toString() {
		return "Person [name=" + name + ", skill=" + skill + "]";
	}
}
