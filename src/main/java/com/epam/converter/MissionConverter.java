package com.epam.converter;

import org.springframework.core.convert.converter.Converter;

import com.epam.model.Mission;

public class MissionConverter implements Converter<String, Mission>{
	
	public Mission convert(final String text){
		String[] values = text.split(",");
		
		final String name = values[0];
		final int strengthNeeded = Integer.valueOf(values[1]);
		
		return new Mission(name, strengthNeeded);
	}
}
