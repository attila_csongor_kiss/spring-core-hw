package com.epam;

import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.support.AbstractApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.epam.model.Pokemon;

public class App 
{
	private static final Logger logger = LoggerFactory.getLogger(App.class);
	
    public static void main( String[] args )
    {
    	final AbstractApplicationContext context = (AbstractApplicationContext) new ClassPathXmlApplicationContext(
				new String[] { "bean.xml" });
    	
    	final Map<String, Pokemon> pokemons = context.getBeansOfType(Pokemon.class);
		
		for(Map.Entry<String, Pokemon> e : pokemons.entrySet()){
			logger.info(e.getValue().toString());
		}
    }
}
